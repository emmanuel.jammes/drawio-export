package com.ratpsmartsystems;

import java.io.IOException;
import java.nio.file.Path;

public interface ExporterInterface {
    void syncFile(Path file) throws IOException;
}
