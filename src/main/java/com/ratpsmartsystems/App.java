package com.ratpsmartsystems;

import java.io.File;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class App extends Thread {

    private final static String drawioExtension = ".drawio";
    private final static String defaultPath = ".";
    private static App app = null;

    public static void main(String[] args) {

        app = new App();
        app.start();

        Runtime.getRuntime().addShutdownHook(new Thread("shutdown thread") {
            public void run() {
                Logger.log("shutting down");
                try {
                    app.interrupt();
                    app.join(2000);
                    if (app.isAlive()) {
                        Logger.log("error stopping main thread");
                    } else {
                        Logger.log("exiting");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void run() {
        try {
            DrawIOExporter drawIOExporter = new DrawIOExporter();

            Path path = Paths.get(defaultPath);

            // startup sync
            List<Path> files = Files.list(path)
                    .collect(Collectors.toList());

            for (Path file : files) {
                if (file.getFileName().toString().endsWith(drawioExtension)) {
                    drawIOExporter.syncFile(file);
                }
            }

            WatchService watchService = FileSystems.getDefault().newWatchService();
            WatchKey key = path.register(watchService,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.ENTRY_DELETE);
            Logger.log("starting watch");

            HashMap<String, LocalDateTime> fileProcessDateTime = new HashMap<>();
            List<Path> filesToProcess = new ArrayList<>();

            while (!isInterrupted()) {

                filesToProcess.clear();
                WatchKey queuedKey = watchService.take();
                for (WatchEvent<?> watchEvent : queuedKey.pollEvents()) {
                    Path file = path.resolve((Path) watchEvent.context());
                    String filename = file.getFileName().toString();

                    if (filename.endsWith(drawioExtension)) {
                        LocalDateTime now = LocalDateTime.now();
                        if (fileProcessDateTime.containsKey(filename)) {
                            // check if file older than n seconds
                            LocalDateTime lastAddedDateTime = fileProcessDateTime.get(filename);
                            if (now.isAfter(lastAddedDateTime.plusSeconds(10))) {
                                fileProcessDateTime.replace(filename, now);
                                filesToProcess.add(file);
                            }
                        } else {
                            // add file in list
                            fileProcessDateTime.put(filename, now);
                            filesToProcess.add(file);
                        }
                    }
                }
                queuedKey.reset();

                // process file list
                for (Path file : filesToProcess) {
                    drawIOExporter.syncFile(file);
                }
            }
        } catch (InterruptedException e) {
            Logger.log("stopping watch");
        } catch (Exception e) {
            Logger.log("error in main thread : " + e);
        }
    }
}
