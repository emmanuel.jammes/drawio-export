package com.ratpsmartsystems;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

public class DrawIOExporter implements ExporterInterface {
    String outputFormat = "png";
    Float scale = 2.0F;
    String drawioExe = "draw.io.exe";
    String drawIOPath = "C:\\Program Files\\draw.io";
    String drawioExePath;

    public DrawIOExporter() {
        String path = System.getenv("DRAWIO");
        if (!path.isEmpty()) {
            drawIOPath = path;
        }
        drawioExePath = drawIOPath + "\\" + drawioExe;
        Logger.log("drawIO=" + drawioExePath);
    }

    private String quote(String str) {
        return "\"" + str + "\"";
    }

    private String getExportFilename(Path file) {
        String filename = file.getFileName().toString();
        String fileNameWithOutExt = filename.replaceFirst("[.][^.]+$", "");
        return fileNameWithOutExt + "." + outputFormat;
    }

    public void syncFile(Path file) throws IOException {
        Path outputFile = Paths.get(getExportFilename(file));

        if (!Files.exists(file)) {
            Logger.log("removing " + outputFile);
            if (Files.exists(outputFile)) {
                try {
                    Files.delete(outputFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            boolean syncNeeded = false;

            if (Files.exists(outputFile)) {
                BasicFileAttributes attrFile = Files.readAttributes(file, BasicFileAttributes.class);
                BasicFileAttributes attrOutputFile = Files.readAttributes(outputFile, BasicFileAttributes.class);
                if (attrFile.lastModifiedTime().toInstant().isAfter(attrOutputFile.lastModifiedTime().toInstant())) {
                    syncNeeded = true;
                }
            } else {
                syncNeeded = true;
            }

            if (syncNeeded) {
                exportFile(file, outputFile);
            }
        }
    }
    private void exportFile(Path file, Path outputFile) throws IOException {
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");

        if (!Files.exists(file)) {
            Logger.log("error : file " + file + " does not exists");
            return;
        }

        String filename = file.getFileName().toString();
        String outputFilename = outputFile.getFileName().toString();
//        String userDirectory = System.getProperty("user.dir");

        if (isWindows) {
            String cmd = "cmd /c " + quote(
                    quote(drawioExePath) +
                        " --export " + quote(filename ) +
                        " --scale " + scale.toString() +
                        " --format " + outputFormat +
                        " --output " + quote(outputFilename)
                    );
            Logger.log("converting to " + outputFormat + " (scale " + scale + ") " + filename);
            Runtime.getRuntime().exec(String.format("cmd.exe /c %s", cmd));
        }
    }
}
